package ru.smlab.xmlparser;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class XmlParserTest {

    @Test
    public void testSingleTag() {
        String input = "<a></a>";
        String expectedTag = "a";


        List<String> parseResults = XMLParser.extractTags(input);

        Assert.assertNotNull(parseResults);
        Assert.assertFalse(parseResults.isEmpty());
        Assert.assertTrue(parseResults.contains(expectedTag));
    }
}
