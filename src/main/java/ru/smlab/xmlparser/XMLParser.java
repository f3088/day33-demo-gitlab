package ru.smlab.xmlparser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLParser {

    public static List<String> extractTags(String input){
        String filtered;
        Pattern p = Pattern.compile("<(\\w+?)></(\\w+?)>");
        Matcher m = p.matcher(input);

        List<String> tags = new ArrayList<>();

        while (m.find()) {
            //првоерить парность тегов
            tags.add(m.group(1));
            tags.add(m.group(2));
        }

        return tags;

    }

}
